#!/usr/bin/python

from sshexpect import * 
from os import system

user="user"
password="password"
host="host"

print "Connecting.." 
s=SSHExpect()
s.connect(user,password, host)
print "Waiting for promt..",
s.waitPrompt()
print "OK"
print "Invoking iperf server" 
s.sendline("iperf -s")
s.expect(["Server listening"])
print "Server listening.. launching client"
system("iperf -c %s" % host)
print "Done"
print "Sending break"
s.sendBreak()
print "Waiting for promt..",
s.waitPrompt()
print "OK"
print "Logout"
s.logout()
