#!/usr/bin/python

from sshexpect import * 
from time import sleep

user="user"
password="pass"
host="host"

print "Connecting.." 
s=SSHExpect()
s.connect(user,password, host)
print "Waiting for promt..",
s.waitPrompt()
print "OK"
print "Invoking yes" 
s.sendline("yes")
s.expect(["y"])
print "Sleeping 10sec"
sleep(10)
s.expect(["y"])
print "Sending break"
s.sendBreak()
print "Waiting for promt..",
s.waitPrompt()
print "OK"
print "Logout"
s.logout()
